<?php
namespace App\Http\Services;

use App\Http\Repositories\UserRepository;

class UserService {

    protected $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    public function getAllUsers($page){

        $users = $this->userRepository->getAllUsers($page);

        return $users;
    }

    public function getOverallUserPosition($id, $numberOfRecord){

        $users = $this->userRepository->getOverallUserPosition($id, $numberOfRecord);

        return $users;
    }

}
