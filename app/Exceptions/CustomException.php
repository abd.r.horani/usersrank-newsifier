<?php

namespace App\Exceptions;

use Exception;
use App\Http\Utility;
class CustomException  extends Exception{

    protected $msg;
    protected $code;

    public function __construct($message, $code = 400){
        $this->msg = $message;
        $this->code = $code;
    }

    public function render($request){
        return [
            'msg' => $this->msg,
            'success' => false,
            'data' => null,
            'status' => $this->code
        ];
    }

}
