<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\UserService;
use App\Http\Resources\UserResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Models\User;

class UserController extends Controller
{

    protected $userService;

    public function __construct(
        UserService $userService
    )
    {
        $this->userService = $userService;
    }


    public function getUsersList(Request $request, $page){

        try{
            $users = $this->userService->getAllUsers($page);
        } catch (\Exception $e) {
            throw new CustomException($e->getMessage());
        }
        return view('usersList',
            compact('users')
        );
    }


    public function getOverallUserPosition(Request $request, $id){

        try{
            $users = $this->userService->getOverallUserPosition($id,
                isset($request->numberOfRecord) ? $request->numberOfRecord : 2);
        } catch (\Exception $e) {
            throw new CustomException($e->getMessage());
        }
        return view('usersList',
            compact('users')
        );
    }

}
