<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\API\UserController;

Route::group([
    'prefix'     => 'v1',
    'namespace'  => 'Botble\RealEstate\Http\Controllers\API',
], function () {
    Route::get('/user/{id}/karma-position', [UserController::class, 'getOverallUserPosition'])->name('get.Overall.User.Position');
});
