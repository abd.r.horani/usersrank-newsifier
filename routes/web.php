<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\UserController;

Route::get('/', function () {
    return redirect('/1');
});
Route::get('/{page}',[UserController::class, 'getUsersList'])->name('users.list');
Route::get('/userOverallPosition/{id}',[UserController::class, 'getOverallUserPosition'])->name('user.position');
