<?php

namespace App\Http\Repositories;


use App\Models\User;

class UserRepository{

    public function getAllUsers($page){
        if($page <= 0){
            $page = 1;
        }
        return User::with('image')
            ->skip(($page - 1) * 100)
            ->take(100)
            ->get();
    }

    public function getOverallUserPosition($id, $numberOfRecord){

        $user = User::with('image')
            ->where('id', $id)
            ->first();

        if($user) {
            $usersSmallerCount = User::where('id', '!=', $id)
                ->where('karma_score', '<=', $user->karma_score)
                ->orderBy('karma_score', 'DESC')
                ->count();
            $usersGreaterCount = User::where('id', '!=', $id)
                ->where('karma_score', '>=', $user->karma_score)
                ->orderBy('karma_score', 'ASC')
                ->count();

            $addGreaterQueryBy = 0;
            $addSmallerQueryBy = 0;
            if($usersSmallerCount < $numberOfRecord){
                $addGreaterQueryBy = $numberOfRecord - $usersSmallerCount;
            }
            if($usersGreaterCount < $numberOfRecord){
                $addSmallerQueryBy = $numberOfRecord - $usersGreaterCount;
            }

            $usersSmaller = User::with('image')
                ->where('id', '!=', $id)
                ->where('karma_score', '<=', $user->karma_score)
                ->limit($numberOfRecord + $addSmallerQueryBy)
                ->orderBy('karma_score', 'DESC')
                ->get();
            $exactuser = User::with('image')
                ->where('id', $id)
                ->get();
            $usersGreater = User::with('image')
                ->where('id', '!=', $id)
                ->where('karma_score', '>=', $user->karma_score)
                ->limit($numberOfRecord + $addGreaterQueryBy)
                ->orderBy('karma_score', 'ASC')
                ->get();

            $merged = $usersGreater->reverse()->merge($exactuser);
            $merged = $merged->merge($usersSmaller);
            $result = $merged->all();
            return $result;
        }else{
            return null;
        }
    }

}
