<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User;

class UserFactory extends Factory
{
    protected $model = User::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = \App\Models\Image::first();
        return [
            'username' => $this->faker->name(),
            'karma_score' => $this->faker->numberBetween(0, 10000),
            'image_id' => $image->id,
        ];
    }
}
