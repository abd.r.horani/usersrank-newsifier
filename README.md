


## About Laravel

`php:8.0` `laravel:8.12`


Laravel project About Users rank :

- api to get user overall position
- using mysql db
- returning imageUrl
- using factory and seed
- customizable number of returned users by api
- simple user interface to list all user and specific user

## Installation 

open a new directory and write the following commends

```
git clone https://gitlab.com/abd.r.horani/usersrank-newsifier.git
```

```
cd usersrank-newsifier
```

```
cp .env.example .env
```
create database and name it 'newsifierdb'

```
composer install
```

```
php artisan migrate
```

```
php artisan db:seed
```
wait a little and then 

```
php artisan serve
```
then you can browse the application : on 

[localhost:8000/](localhost:8000/)

you will find a simple table that lists all users and you can click on each user to get 2 higher and lower 

also you can use the api ( the postman collection exist in the top directory of the project ) 

```
localhost:8000/api/v1/user/2/karma-position?numberOfRecord=2
```
note : numberOfRecord=2 mean that 2 higher and 2 lower

 
 appriciate your time
   
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
