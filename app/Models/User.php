<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'karma_score',
        'image_id',
    ];

    protected $appends = ['position'];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function getPositionAttribute(){
        return User::where('karma_score', '>=', $this->karma_score)->where('id', '!=', $this->id)->count();
    }

}
