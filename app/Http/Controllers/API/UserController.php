<?php

namespace App\Http\Controllers\API;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\UserService;
use App\Http\Resources\UserResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Models\User;

class UserController extends Controller
{

    protected $userService;

    public function __construct(
        UserService $userService
    )
    {
        $this->userService = $userService;
    }


    public function getOverallUserPosition(Request $request, $id){

        try{
            $users = $this->userService->getOverallUserPosition($id,
                isset($request->numberOfRecord) ? $request->numberOfRecord : 2);
            if($users) {
                return UserResource::collection($users);
            }else{
                throw new HttpException(400, 'user not found');
            }
        } catch (\Exception $e) {
            throw new CustomException($e->getMessage());
        }
    }

}
