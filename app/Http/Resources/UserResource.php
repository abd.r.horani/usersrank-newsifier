<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'msg' => 'success',
            'success' => true,
            'data' => [
                'id' => $this->id,
                'username' => $this->username,
                'karma_score' => $this->karma_score,
                'position' => $this->position,
                'image' => $this->image->url,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ],
            'status' => 200

        ];
    }
}
