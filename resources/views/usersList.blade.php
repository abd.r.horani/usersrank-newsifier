@extends('welcome')

@section('style')

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        tr{
            cursor: pointer;
        }

        tr:hover{
            background-color: #e2e2e29e;
        }

        .user_img{
            width: 40px;
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    <table>
        <tr>
            <th>ID</th>
            <th>User Name</th>
            <th>Position</th>
            <th>Karma Score</th>
        </tr>
        <tbody>
            @foreach($users as $user)
                <tr data-id="{{$user->id}}" class="user">
                    <td>{{$user->id}}</td>
                    <td>
                        <img class="user_img" src="{{$user->image->url}}">
                        {{$user->username}}
                    </td>
                    <td>{{$user->position}}</td>
                    <td>{{$user->karma_score}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection

@section('script')
    <script>
    $(document).ready(function (){
        $('.user').click(function(){
            var id = $(this).data('id');
            window.location.href = window.location.origin + '/userOverallPosition/' + id;
        });
    });
    </script>
@endsection
